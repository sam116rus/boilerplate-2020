import {slideToggle} from "./slide-toggle";
import viewport from "./viewport";

export default function () {

    let tabsWarp = document.getElementsByClassName('tabs');

    Array.from(tabsWarp).forEach(function (currentTabsWarp) {

        let Tabs = currentTabsWarp.getElementsByClassName('js-tabs-controls__item');
        let AdaptiveTabs = currentTabsWarp.getElementsByClassName('tabs-content__item-title');

        let Panes = currentTabsWarp.getElementsByClassName('tabs-content__item');

        Array.from(Tabs).forEach(tab => tab.addEventListener('click', function(e){

            let index = Array.from(Tabs).indexOf(this);

            [].forEach.call(Tabs, function(el) {
                el.classList.remove("is-active");
            });

            [].forEach.call(Panes, function(el) {
                el.classList.remove("is-active");
            });

            this.classList.add('is-active');

            Panes[index].classList.add('is-active');

            e.preventDefault();
        }));


        Array.from(AdaptiveTabs).forEach(tab => tab.addEventListener('click', function(e){

            let index = Array.from(AdaptiveTabs).indexOf(this);

            this.classList.toggle('is-active');

            Panes[index].classList.toggle('is-active');

            slideToggle(Panes[index], 500);

            e.preventDefault();
        }));



    });


    let tabsWrap = document.getElementsByClassName('page-tabs-nav')[0];

    if(viewport().width < 767 && tabsWrap){
        let activeTab = tabsWrap.getElementsByClassName('is-active')[0];

        tabsWrap.scrollLeft = activeTab.offsetLeft - 20;
    }

}
