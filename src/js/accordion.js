import {slideToggle} from "./slide-toggle";

export default function() {

    let accordionTitle = document.getElementsByClassName('js-accordion__title');

    Array.from(accordionTitle).forEach(button => button.addEventListener('click', function(e){
        slideToggle(this.nextElementSibling, 500);
        this.parentNode.classList.toggle('is-opened');
        e.preventDefault();
    }));
}
