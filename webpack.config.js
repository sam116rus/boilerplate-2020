const path = require('path')

module.exports = {
    entry: {
        scripts: './src/js/main.js',
        backend: './src/js/backend.js'
    },
    output: {
        path: path.resolve(__dirname, './dist/js'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    devtool: 'eval-source-map',
    optimization: {
        usedExports: true
    },
    mode: 'development',
    externals: {},
    plugins: []
}
