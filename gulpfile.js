const gulp = require('gulp'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    cssMinify = require('gulp-csso'),
    del = require('del'),
    newer = require('gulp-newer'),
    svgSprite = require('gulp-svg-sprite'),
    prettier = require('gulp-prettier'),
    cache = require('gulp-cached'),
    webpack = require('webpack'),
    webpackConfig = require('./webpack.config.js'),
    mergeMediaQurires = require('gulp-merge-media-queries'),
    webpackStream = require('webpack-stream'),
    gulpImagemin = require('gulp-imagemin'),
    imageminPngquant = require('imagemin-pngquant'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    fileInclude = require('gulp-file-include'),
    projectMap = require('gulp-project-map'),
    vinylPaths = require('vinyl-paths');


const browserSync = require('browser-sync').create();

// Paths for reuse
const srcPath = (file, watch = false) => {
    if (file === 'scss' && watch === false) return './src/scss/styles.scss';
    if (file === 'scss' && watch === true) return './src/scss/**/*.scss';
    if (file === 'js' && watch === false) return entryArray;
    if (file === 'js' && watch === true) return './src/js/**/*.js';
    if (file === 'html' && watch === true) return './src/**/*.html';
    if (file === 'html' && watch === false) return './src/*.html';
    if (file === 'assets') return './src/assets/**/*';
    if (file === 'img') return './src/img/**/*';
    if (file === 'sprite') return './src/img/icons/*.svg';
    console.error('Unsupported file type entered into Gulp Task Runner for Source Path');
};
const distPath = (file, serve = false) => {
    if (['css', 'js', 'img'].includes(file)) return `./dist/${file}`;
    if (file === 'html' && serve === false) return './dist/**/*.html';
    if (file === 'html' && serve === true) return './dist';
    if (file === 'assets') return './dist/assets/';
    console.error('Unsupported file type entered into Gulp Task Runner for Dist Path');
};

gulp.task('sprite', function() {
    return gulp
        .src(srcPath('sprite'))
        .pipe(
            svgSprite({
                mode: {
                    inline: true, // Prepare for inline embedding
                    symbol: {
                        sprite: "../sprite.svg"
                    }
                }
            }),
        )
        .pipe(gulp.dest('./src/img'));
});


gulp.task('clean-html', function(done) {
    del([distPath('html')])
    done();
});


gulp.task('html', function() {
    return gulp
        .src(srcPath('html', false))
        .pipe(cache('html'))
        .pipe(fileInclude({
            prefix: '@@',
        }))
        .pipe(gulp.dest(distPath('html', true)))
        .pipe(browserSync.stream());
});


gulp.task('project-map', function(done) {
    projectMap({
        path: './dist/',
        extension: '.html',
        name: 'index',
        title: 'project-map'
    });
    done();
});


gulp.task('assets', function() {
    return gulp
        .src('./src/assets/**/*')
        .pipe(newer('./dist/assets'))
        .pipe(gulp.dest('./dist/assets'))
        .pipe(browserSync.stream());
});

gulp.task('images', function() {
    return gulp
        .src(srcPath('img'))
        .pipe(gulpImagemin([
            gulpImagemin.gifsicle(),
            gulpImagemin.jpegtran(),
            gulpImagemin.optipng(),
            gulpImagemin.svgo(),
            imageminPngquant(),
            imageminJpegRecompress(),
        ]))
        .pipe(gulp.dest(distPath('img')))
});


gulp.task('styles', function() {
    return gulp
        .src(srcPath('scss'))
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(mergeMediaQurires())
        .pipe(gulp.dest(distPath('css')))
        .pipe(cssMinify())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest(distPath('css')))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
    return gulp
        .src(srcPath('js', true))
        .pipe(plumber())
        .pipe(webpackStream(webpackConfig, webpack))
        .pipe(gulp.dest(distPath('js')))
        .pipe(browserSync.stream());
});

gulp.task('scripts-production', function() {
    return gulp
        .src(srcPath('js', true))
        .pipe(plumber())
        .pipe(webpackStream({ ...webpackConfig, mode: 'production', devtool: 'source-map' }, webpack))
        .pipe(gulp.dest(distPath('js')))
        .pipe(browserSync.stream());
});

gulp.task('clean', function() {
    return del('./dist');
});

gulp.task('serve', function() {
    browserSync.init({
        port: 3000,
        host: "192.168.31.73",
        ghostMode: false,
        server: distPath('html', true)
    });
    gulp.watch(srcPath('html', true), gulp.series('clean-html', 'html'));
    gulp.watch(srcPath('sprite', true), gulp.series('sprite', 'clean-html', 'html'));
    gulp.watch(srcPath('scss', true), gulp.series('styles'));
    gulp.watch(srcPath('js', true), gulp.series('scripts'));
    gulp.watch(srcPath('img', true), gulp.series('images'));
    gulp.watch(srcPath('assets', true), gulp.series('assets'));
});


gulp.task('dev', gulp.series('clean', 'images', 'sprite', 'html', gulp.parallel('assets', 'styles', 'scripts')));

gulp.task('build', gulp.series('clean', 'images', 'sprite', 'html', 'project-map', gulp.parallel('assets', 'styles', 'scripts-production')));

gulp.task('default', gulp.series('build', 'serve'));
